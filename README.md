# itk-demo-template

ITk demonstrator GUI React app + flask server + celery worker template.

V0.1-alpha

This template sets up a micro service with a React/JavaScript frontend
and a Flask/Python app with a Celery task queue.

Frontend and app are connected through a REST API described in OpenAPI 3.0.

The app consists of a Flask webserver which is served by gunicorn.

The Celery task queue consists of a celery app (= configured celery library and tasks)
and a celery worker, running separately.

The frontend is based on `create-react-app` and uses Patternfly 4 (which is based
on Bootstrap 4) components and layouts.

[JavaScript](https://www.w3schools.com/js/DEFAULT.asp) |
[React.js](https://reactjs.org/) |
[Patternfly](https://www.patternfly.org/v4/) |
[Bootstrap](https://getbootstrap.com/)

The flask app was adapted from [this blogpost](https://mark.douthwaite.io/getting-production-ready-a-minimal-flask-app/).

Frontend and backend are connected through a REST API described in OpenAPI 3.0.
The API description file is automagically converted to flask routes using the
Python module `connexion`.

[OpenAPI 3.0](https://swagger.io/docs/specification/about/) |
[connexion](https://github.com/zalando/connexion)

## Installation

1. Clone git directory.

         `git clone https://gitlab.cern.ch/itk-demo-sw/itk-demo-template.git`

1. Navigate to the root directory `/itk-demo-template`

### React Frontend

1. Install and build the React toolchain based on `create-react-app`

   ```
   nvm use stable
   # npx create-react-app react-app
   # cd react-app
   npm i
   npm run build
   ```

   You can test the transpiled static React app alone using

         `serve -s build`

1. Deploy React app to directory defined in Flask app

         `mv -r build ..`

1. Install Patternfly 4 package

         `npm install @patternfly/react-core --save`

### Flask app

1. The Flask Python app lives in the `template` directory.
Rename eventually to your plugins name.

1. [OPTIONAL] Create a virtual enviroment and activate it.

   ```
   python3 -m venv venv
   source venv/bin/activate
   pip install -r requirements.txt
   ```

2. Start the [gunicorn](https://gunicorn.org/) web server.

         `bash bin/run.sh`

3. You can now access the server in your browser

   `http://127.0.0.1:5000/` for the React app

   `http://127.0.0.1:5000/api` for the Flask app

   By default the Swagger UI is shown at the root URL, automatically created from the openAPI file.
   Here you are able to test the endpoints.

4. To start development of your own:
   1. Create an empty repo on gitlab in the `itk-demo-sw` group.
   1. Clone it.
   1. Copy the template contents to the cloned empty repo.
   1. Rename the `template` module to the name of your module (directory name and all occurences in openapi definition and imports).
   1. Push it.

Should you have any problems/comments on the setup of the server or the implementation of your plugin feel free to contact us via <jonas.schmeing@cern.ch>
or <gbrandt@cern.ch>

## Running

start the celery worker

```shell
./bin/run_worker.sh
```

start the gunicorn wsgi server serving the flask app

```shell
./bin/run_server.sh
```

## Template Contents

- `bin/` Binary tools, for now only contains
  - `run.sh` script to start [gunicorn](https://gunicorn.org/) web server.
- `openapi` OpenAPI definiton location.
  - `openapi.yaml` define your endpoints here. see [OpenAPI specification](https://oai.github.io/Documentation/specification.html) for syntax.
- `template` Python module of your flask server.
  - `__init__.py` Needed by Python to mark directory as module.
  - `app.py` App factory, this creates a [Flask](https://flask.palletsprojects.com/) app automatically out of the OpenAPI defintion using the[connexion](https://github.com/zalando/connexion) module.
  - `routes.py` Routes mentioned in OpenAPI definiton.
  - `backend.py` contains calls to your backend (from the routes). eg. python C++ bindings. Just included for demonstration that this part does not have to be part of the flask server anymore.

## TO DO

- Add asynchronous task example using Celery task queue
- Add Patternfly4 GUI template
  - GUI sync with other modules
