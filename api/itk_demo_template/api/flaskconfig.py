#
# itk-demo-template microservice celery configuration
#
# Celery configuration settings described here:
# https://docs.celeryq.dev/en/master/userguide/configuration.html
#
# 2022 G. Brandt <gbrandt@cern.ch>
#

import logging
from logging_tree import printout

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

logger.info("loading Flask config")

# printout()
