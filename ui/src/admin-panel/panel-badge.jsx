import React, { useState } from 'react'
import {
  Bullseye,
  Button,
  Card,
  CardTitle,
  CardBody,
  Checkbox,
  Grid,
  GridItem,
  TextInput
} from '@patternfly/react-core'
import {
  CogIcon
} from '@patternfly/react-icons'
import { useAdminConfig } from './config'
import PropTypes from 'prop-types'

const PanelBadge = (props) => {
  const [showCard, setShowCard] = useState(false)
  const { fields, reset } = useAdminConfig()

  const marginTop = props.marginTop ? props.marginTop : 5
  const marginRight = props.marginRight ? props.marginRight : 5
  const distanceToBadge = props.distanceToBadge ? props.distanceToBadge : 10
  const badgeSize = props.badgeSize ? props.badgeSize : 50
  const button = (
    <Button
      key={'panel badge button button'}
      variant={'plain'}
      style={{
        position: 'fixed',
        top: `${marginTop}px`,
        zIndex: 100000000,
        float: 'right',
        right: `${marginRight}px`,
        cursor: 'pointer',
        verticalAlign: 'middle',
        margin: 'auto'
      }}
      onClick={() => setShowCard(!showCard)}
    >
      <CogIcon
        key={'panel badge button icon'}
        style={{
          fontSize: `${badgeSize}px`,
          zIndex: 100000000,
          color: 'grey',
          borderRadius: '50%',
          backgroundColor: '#cfdcec',
          border: '7px solid #cfdcec'
        }}
      />
    </Button>
  )

  const card = (
    <Card
      key={'panel badge card card'}
      style={{
        position: 'fixed',
        top: `${distanceToBadge + marginTop + badgeSize}px`,
        zIndex: 100000000,
        float: 'right',
        right: `${marginRight}px`,
        cursor: 'pointer',
        verticalAlign: 'middle',
        margin: 'auto',
        width: '20%'
      }}
    >
      <CardTitle key={'panel badge card title'}>
        <h1
          key={'panel badge card h1'}
          style={{
            fontSize: '20px'
          }}
        >
          {'GUI Configuration'}
        </h1>
      </CardTitle>
      <CardBody key={'panel badge card body'}>
        <Grid key={'panel badge card body grid'} hasGutter>
          {fields.map(
            field => {
              const name = (
                <div style={{ fontWeight: 'bold' }} key={'name ' + field.key}>
                  {field.title}
                </div>
              )
              const setting = field.type === 'boolean'
                ? (
                    <Checkbox
                      key={'checkbox' + field.key}
                      value={field.value}
                      label={field.key}
                      onChange={field.set}
                    />
                  )
                : (
                    <TextInput
                      key={'textinput' + field.key}
                      value={field.value}
                      label={field.key}
                      onChange={field.set}
                      aria-label={'TextInput ' + field.key}
                    />
                  )
              return (
                <GridItem key={'panel badge card body grid item name ' + field.key} span={12}>
                  {name}
                  {setting}
                </GridItem>
              )
            }
          )}
          <GridItem key={'panel badge card body grid item button'}>
            <Bullseye key={'panel badge card reset bullseye'}>
              <Button
                key={'panel badge card reset button'}
                onClick={reset}
              >
                Reset config
              </Button>
            </Bullseye>
          </GridItem>
        </Grid>
      </CardBody>
    </Card>
  )

  return (
    <>
      {button}
      {showCard ? card : null}
    </>
  )
}

PanelBadge.propTypes = {
  marginTop: PropTypes.number,
  marginRight: PropTypes.number,
  distanceToBadge: PropTypes.number,
  badgeSize: PropTypes.number
}

export default PanelBadge
