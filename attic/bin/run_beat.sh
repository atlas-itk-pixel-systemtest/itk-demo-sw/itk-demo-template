#!/usr/bin/env bash

# if command -v redis-cli &> /dev/null ; then
#   pong=$(redis-cli -h redis ping)
# else
#   echo "redis-cli not available. Please install."
#   exit 1
# fi

# if [ "$pong" = "PONG" ] ; then
#   echo "$pong: REDIS available"
# else
#   echo "REDIS not available. Please start REDIS server."
#   exit 1
# fi

# #shellcheck disable=SC2034
# CELERY_BROKER_URL=redis://redis/0
# #shellcheck disable=SC2034
# CELERY_RESULT_BACKEND=redis://redis/0

echo "parameters: $@"

if command -v celery &> /dev/null ; then
  celery -A itk_demo_template beat
else
  if command -v demi &> /dev/null ; then
    demi python run celery -A itk_demo_template beat -l DEBUG "$@"
  else
    echo "demi not available. Please set it up."
    exit 1
  fi
fi


