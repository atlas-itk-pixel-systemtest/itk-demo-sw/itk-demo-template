#!/usr/bin/env bash

# GUNICORN

gcmd() {
  np=$(nproc)
  nw=$((2*np+1))
  nw=4
  gunicorn wsgi:app --bind 0.0.0.0:5001 --workers=$nw --preload
  # --reload
}

if command -v gunicorn -v &> /dev/null
then
    gcmd
else
  if command -v demi &> /dev/null
  then
        echo "gunicorn not available - activate your venv?"
        exit 1
        # demi python run gcmd
  else
    echo "demi and/or gunicorn not available. Please set it up."
    exit 1
  fi
fi


# uWSGI
# uwsgi --http :9090 --wsgi-file wsgi.py --callable app
