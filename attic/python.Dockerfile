
FROM python:slim as python
ENV PYTHONUNBUFFERED=true
WORKDIR /app

# ARG USERNAME=demi
# ARG PORT=5001
# RUN useradd $USERNAME

FROM python as poetry
ENV POETRY_HOME=/opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV PATH="$POETRY_HOME/bin:$PATH"
RUN python -c 'from urllib.request import urlopen; print(urlopen("https://install.python-poetry.org").read().decode())' | python -
COPY . ./
RUN poetry install --without dev --no-interaction --no-ansi -vvv
RUN ls

FROM python as runtime
ENV PATH="/app/.venv/bin:$PATH"
RUN ls -lrht
COPY --from=poetry /app /app
EXPOSE $PORT
CMD flask run

