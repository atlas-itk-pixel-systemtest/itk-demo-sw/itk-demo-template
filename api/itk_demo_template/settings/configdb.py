import config_checker as cc
import json, os
from itk_demo_template.settings.settings_classes import Settings, Config, ConfigSupport, FILE_DIR
import logging

TYPE = "felix"
logger = logging.getLogger(__name__)

# Create the directory if it does not exist
os.makedirs(FILE_DIR, exist_ok=True)

# script to load settings from configdb into environment variables and write config and settings file to plate
# will check validity of envvars and config file if configdb is not used


class UseConfigDB(cc.BaseConfig):
    use_configdb: bool = False

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)


def main():
    if UseConfigDB().use_configdb:
        config_support = ConfigSupport()
        config = Config(
            config_sources=[
                cc.ConfigDBJsonSource(
                    config_support.configdb_key,
                    config_support.runkey,
                    {"search_dict": json.loads(config_support.search_dict), "object_type": TYPE, "config_type": "config"},
                ),
            ]
        )

        with open(f"{FILE_DIR}/config.json", "w") as f:
            f.write(json.dumps(config.model_dump()))  # .dict() for older pydantic versions

        settings = Settings(
            config_sources=[
                cc.ConfigDBJsonSource(
                    config_support.configdb_key,
                    config_support.runkey,
                    {"search_dict": json.loads(config_support.search_dict), "object_type": TYPE, "config_type": "meta"},
                )
            ]
        )

    else:
        settings = Settings(config_sources=cc.EnvSource(allow_all=True))

        file = f"{FILE_DIR}/{settings.config_file}"
        try:
            with open(file) as f:
                try:
                    json.load(f)
                    config = Config(config_sources=cc.FileSource(file=f"{FILE_DIR}/{settings.config_file}"))
                except Exception:
                    logger.warning(f"Config file {file} could not be checked, because it is not in json format.")
        except FileNotFoundError:
            logger.error(f"Config file not found: {file}")

    settings_dict = settings.model_dump()  # .dict() for older pydantic versions

    for key, value in settings_dict.items():
        print(key.upper())
        print(value)

    with open(f"{FILE_DIR}/settings.json", "w") as f:
        f.write(json.dumps(settings_dict))


if __name__ == "__main__":
    main()
