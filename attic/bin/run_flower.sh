#!/usr/bin/env bash

if command -v celery &> /dev/null
then
  celery -A itk_demo_template flower --broker_api=http://guest:guest@rabbitmq:15672/api/
else
  if command -v demi &> /dev/null
  then
      echo "celery not available - activate your venv?"
      exit 1
  # demi python run celery -A itk_demo_template worker -E -l INFO
  else
    echo "demi not available. Please set it up."
    exit 1
  fi
fi


