#!/usr/bin/env bash

if command -v celery &> /dev/null
then
  cat celeryconfig.py
  celery -A itk_demo_template worker
  # -l DEBUG
  # —without-gossip —without-mingle —without-heartbeat
else
  if command -v demi &> /dev/null
  then
      echo "celery not available - activate your venv?"
      exit 1
  # demi python run celery -A itk_demo_template worker -E -l INFO
  else
    echo "demi not available. Please set it up."
    exit 1
  fi
fi


