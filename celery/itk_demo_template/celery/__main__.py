#
# celery example for template microservice
#
# Executable to dump celery config
#
# 2022 G. Brandt <gbrandt@cern.ch>
#

import sys
import logging

# from pprint import pprint

from . import celery_app

logging.basicConfig(level=logging.NOTSET)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def main():
    # celery_app.logger.info("Celery app:")
    # print(celery_app.conf['Settings'])

    # for k in celery_app.conf.items():
    # print(k)

    # pprint(repr(celery_app.conf))

    for k, v in celery_app.conf.items():
        if not k.startswith("__"):
            logger.info(f" {k} : {v}")


if __name__ == "__main__":
    sys.exit(main())  # next section explains the use of sys.exit
