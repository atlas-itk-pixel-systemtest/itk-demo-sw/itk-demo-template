import logging

import connexion
from connexion.exceptions import ProblemException

from connexion import http_facts, FlaskApp
from connexion.problem import problem
from connexion.lifecycle import ConnexionRequest, ConnexionResponse
from connexion.options import SwaggerUIOptions
from connexion.middleware import MiddlewarePosition
from starlette.middleware.cors import CORSMiddleware

# from connexion.resolver import RelativeResolver
# from connexion.resolver import RestyResolver


logging.basicConfig(level=logging.NOTSET)

# from pprint import pprint

# logger.setLevel(logging.DEBUG)

# from logging_tree import printout

# to do: use Flask logger in case gunicorn logger not available

# ld = logging.Logger.manager.loggerDict
# if "gunicorn.error" in ld.keys():
#     gunicorn_logger = logging.getLogger("gunicorn.error")
#     flask_app.logger.handlers = gunicorn_logger.handlers
#     flask_app.logger.setLevel(gunicorn_logger.level)

# logger = logging.getLogger("gunicorn.error")

log = logging.getLogger(__name__)


def create_app(config=None):
    swagger_ui_options = SwaggerUIOptions(
        swagger_ui=True,
        swagger_ui_path="/docs",
        spec_path="/",
    )
    app = FlaskApp(
        __name__,
        specification_dir="openapi",
        swagger_ui_options=swagger_ui_options,
        # server_args={
        #     "static_folder": "../ui/build",
        #     "static_url_path": "/",
        # },
    )

    # app.add_api(
    #     "openapi.yml",
    #     swagger_ui_options=swagger_ui_options,
    #     # resolver=RestyResolver("api")
    # )

    flask_app = app.app
    # flask_app.config.from_object("itk_demo_template.api.flaskconfig")
    # # pprint(flask_app.config)
    # flask_app.logger.setLevel(logging.INFO)

    # CORS(flask_app)

    app.add_middleware(
        CORSMiddleware,
        position=MiddlewarePosition.BEFORE_EXCEPTION,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    with flask_app.app_context():
        app.add_api(
            "openapi.yml",
            swagger_ui_options=swagger_ui_options,
            # resolver=RestyResolver("api")
        )
    #     #     app.add_api("openapi.yml")

    #     @flask_app.route("/")
    #     def index():
    #         return flask_app.send_static_file("index.html")

    app.add_error_handler(404, not_found_handler)
    app.add_error_handler(405, not_found_handler)
    app.add_error_handler(ProblemException, not_found_handler)

    # flask_app.logger.info(f"{__name__}: app created.")

    return app


def create_flask_app(config=None):
    """entry point for flask development server"""
    app = create_app(config)

    return app.app


def not_found_handler(request: ConnexionRequest, exc: Exception) -> ConnexionResponse:
    return problem(
        title=http_facts.HTTP_STATUS_CODES.get(404),
        detail="The resource was not found",
        status=404,
    )


# def flask_error_response(error):
#     error = Error(
#         error.code,
#         error.name,
#         error.description,
#         instance=flask_error_response.__module__
#         + " - "
#         + flask_error_response.__name__,
#     )
#     return error.to_conn_response()


# def connexion_error_response(error):
#     error = Error(
#         error.status,
#         error.title,
#         error.detail,
#         instance=connexion_error_response.__module__
#         + " - "
#         + connexion_error_response.__name__,
#     )
#     return error.to_conn_response()
