ARG USERNAME=demi
ARG PORT=5001

FROM centos:centos7 as base-centos7
SHELL ["/bin/bash", "--login", "-c"]

# Install dependencies
RUN yum install -y deltarpm
RUN yum install -y epel-release
RUN yum -y update
RUN yum install -y sudo which git jq patch make envsubst redis

RUN sudo yum install -y gcc zlib-devel bzip2 bzip2-devel readline-devel \
sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel gdbm-devel \
ncurses-devel db4-devel wget

#RUN yum -y update
#RUN yum -y upgrade

RUN yum -y clean all

FROM centos:centos7 as user-centos7
ARG USERNAME=demi
ARG PORT=5001
COPY --from=base-centos7 / /
# Add demi user
RUN groupadd $USERNAME
RUN useradd -m -r -g $USERNAME $USERNAME
RUN echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

USER $USERNAME
ENV HOME="/home/${USERNAME}"
WORKDIR ${HOME}
