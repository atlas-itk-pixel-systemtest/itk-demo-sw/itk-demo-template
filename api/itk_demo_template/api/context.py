#
# Module providing shared FSM context for template microservice
#
# 2022 G. Brandt gbrandt@cern.ch
#


import logging
from multiprocessing import Lock, Value

# from flask import current_app

###### draft to implement via multiprocessing Manager

# from multiprocessing.managers import AcquirerProxy, BaseManager, DictProxy

# def get_shared_state(host, port, key):
#     shared_dict = {}
#     shared_lock = Lock()
#     manager = BaseManager((host, port), key)
#     manager.register("get_dict", lambda: shared_dict, DictProxy)
#     manager.register("get_lock", lambda: shared_lock, AcquirerProxy)
#     try:
#         manager.get_server()
#         manager.start()
#     except OSError:  # Address already in use
#         manager.connect()
#     return manager.get_dict(), manager.get_lock()

# HOST = "127.0.0.1"
# PORT = 35791
# KEY = b"secret"
# shared_dict, shared_lock = get_shared_state(HOST, PORT, KEY)
# shared_dict["number"] = 0

logger = logging.getLogger("gunicorn.error")


class Context:
    # lock = Lock()
    states_dict = {}
    vstate = Value("i", 0)
    vcount = Value("i", 0)

    @staticmethod
    def set_state(state):
        # locked = Context.lock.acquire(block=False)
        # with Context.lock:
        with Context.vcount.get_lock():
            Context.vcount.value += 1
            logger.debug(
                f"{__name__} set_state {id(Context.vcount)} {Context.vcount.value}"
            )

        with Context.vstate.get_lock():
            if Context.vstate.value != state:
                Context.vstate.value = state

    @staticmethod
    def counter():
        """Value of internal set_state call counter"""
        return Context.vcount.value

    @staticmethod
    def get_state_name():
        """current state name as string"""
        return Context.states_dict[Context.vstate.value]().name

    @staticmethod
    def get_state_int():
        """current state enum int value"""
        return Context.vstate.value

    @staticmethod
    def get_state_cls():
        """current state class"""
        return Context.states_dict[Context.vstate.value]

    @staticmethod
    def get_state():
        """current state instance"""
        return Context.states_dict[Context.vstate.value]()

    @staticmethod
    def input(msg):
        return Context.get_state_cls().input(msg)

    @staticmethod
    def get_all_state_names():
        """list of all registered state names"""
        state_names = [state().name for state in Context.states_dict.values()]
        return state_names
