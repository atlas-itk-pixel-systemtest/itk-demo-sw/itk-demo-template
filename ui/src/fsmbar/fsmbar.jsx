import React, { useState, useEffect } from 'react';
import {
  Button,
  Flex,
  FlexItem,
  Label,
  Spinner,
} from '@patternfly/react-core';
import PropTypes from 'prop-types';
import { useConfig } from '../admin-panel/config'

const templateApiUrl = process.env.REACT_APP_BASE_API_URL;

export const FsmBar = (props) => {
  // const { getConfig } = useConfig()
  // const templateApiUrl = getConfig('backend')
  // console.log(templateApiUrl);
  const [fsmvar, setFsm] = useState();

  useEffect(() => {
    //const fetchHealth = 
    (async () => {
      const health = await fetch(templateApiUrl+'/health');
      if (health.ok) {      
        const health_json = await health.json();
        setFsm(health_json['fsm']);
      } else {
        setFsm(null);
      }
    })();
    // fetchHealth().catch(console.error)
  }, [])

  return (
    <>
      { fsmvar === undefined ?
      <Spinner isSVG aria-label="Wating for FSM ..." />
        :
          <Flex>
            { fsmvar === null ?
            <p>API not reachable.</p>
            :
            <>
            <Flex>
              {
                fsmvar['all_actions'].map((item, i) => (
                  <FlexItem key={`fsm-flex-${item}`}>
                    <Button
                      //isDisabled={fsmact[i]}
                      // onClick={(async () => {
                      //   await fetch(`${templateApiUrl}/${item}`);
                      // })();}
                    >
                      {item}
                    </Button>
                  </FlexItem>
                ))
                  }
                </Flex>
                <Flex>
                  {
                fsmvar['all_states'].map((item, i) => (
                  <FlexItem key={`fsm-lbl-${item}`}>
                    <Label 
                      // color={fsmstcols[i]}
                    >
                      {item}
                    </Label>
                  </FlexItem>
                ))
            }
            </Flex>
            </>
          }
          </Flex>
      }
    </>
  );
}

FsmBar.propTypes = {
  apiurl: PropTypes.string.isRequired,
};

export default FsmBar
