#!/bin/bash
#
# GitLab API tasks
#
# 2022, Gerhard Brandt <gbrandt@cern.ch>

# take the id from the project website
export GITLAB_PROJECTID=122981

export GITLAB_SERVER=gitlab.cern.ch
GITLAB_TOKEN=$(cat ./.gitlab-token)
export GITLAB_TOKEN
export GITLAB_ROOTGROUP=atlas-itk-pixel-systemtest
export GITLAB_SUBGROUP=itk-demo-sw
export GITLAB_PROJECT=itk-demo-template
export GITLAB_FEATURE=feature/my-feature-0

export GITLAB_API_URL="https://${GITLAB_SERVER}/api/v4/projects/${GITLAB_PROJECTID}"

# GITLAB_URL=http://${GITLAB_SERVER}/${GITLAB_ROOTGROUP}/${GITLAB_PROJECT}

function info_project {
#    curl -L "http://${GITLAB_URL}/repository/files/"
#    echo "${GITLAB_API_URL}"
#    curl --header  "PRIVATE-TOKEN: ${GITLAB_TOKEN}" ${GITLAB_API_URL}/repository/tree | jq '.[].name'
  curl -sS ${GITLAB_API_URL}/repository/tree | jq '.[].name'

}

function create_rootgroup {
  rootGroupId=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_SERVER/api/v4/groups?search=$GITLAB_ROOTGROUP" | jq '.[0]["id"]' ) 
  if [ $rootGroupId == "null" ]; then 
    rootGroupId=$(curl -d "name=$GITLAB_ROOTGROUP&path=$GITLAB_ROOTGROUP&visibility=private&lfs_enabled=true&description=Root group" -X POST "$GITLAB_SERVER/api/v4/groups" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.["id"]') 
  fi
  echo "Root group Id: $rootGroupId"
}

function create_subgroup {
  rootSubGroupId=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_SERVER/api/v4/groups/$rootGroupId/subgroups?search=$GITLAB_SUBGROUP" | jq '.[0]["id"]' ) 
  if [ $rootSubGroupId == "null" ] 
    then rootSubGroupId=$(curl -d "name=$GITLAB_SUBGROUP&path=$GITLAB_SUBGROUP&visibility=private&lfs_enabled=true&description=$GITLAB_SUBGROUP programme&parent_id=$rootGroupId" -X POST "$GITLAB_SERVER/api/v4/groups" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.["id"]') 
  fi
  echo "Sub group Id: $rootSubGroupId"
}

function create_project {
  projectId=$(curl "$GITLAB_SERVER/api/v4/groups/$rootSubGroupId/projects?search=$GITLAB_PROJECT" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.[0]["id"]' )
  if [ $projectId == "null" ]
    then projectId=projectId=$(curl -d "path=$GITLAB_PROJECT&namespace_id=$rootSubGroupId" -X POST "$GITLAB_SERVER/api/v4/projects" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.["id"]') 
  fi 
  echo "Project Id: $projectId"
  cd $GITLAB_PROJECT || exit
  git init 
  git remote add origin
  git@localhost:$GITLAB_ROOTGROUP/$GITLAB_SUBGROUP/$GITLAB_PROJECT.git
  git add .
  git commit -m "Initial commit" 
  git push -u origin master
}

function create_gitflow {
  # Git branches initial structure as per Gitflow
  projectId=$(curl "$GITLAB_SERVER/api/v4/groups/$rootSubGroupId/projects?search=$GITLAB_PROJECT" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.[0]["id"]' )
 
  #create develop branch 
  curl -d "branch=develop&ref=master" -X POST "$GITLAB_SERVER/api/v4/projects/$projectId/repository/branches" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq 
  
  #create feature branch 
  curl -d "branch=$GITLAB_FEATURE&ref=develop" -X POST "$GITLAB_SERVER/api/v4/projects/$projectId/repository/branches" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq
}
