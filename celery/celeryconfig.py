#
# itk-demo-template microservice celery configuration
#
# Celery configuration settings described here:
# https://docs.celeryq.dev/en/master/userguide/configuration.html
#
# 2022 G. Brandt <gbrandt@cern.ch>
#

import os.path
import logging
import socket

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
# logger = logging.getLogger('gunicorn.error')

log.info("celeryconfig.py")

# Time and date settings

enable_utc = True
timezone = "Europe/Paris"

if os.path.exists("/.dockerenv"):
    log.info("Running in container")
    # broker_url = 'redis://redis:6379/0'
    # broker_url='amqp://',
    broker_url = "pyamqp://guest:guest@rabbitmq"
    result_backend = "redis://redis:6379/0"
    # backend='rpc://', # guest:guest@rabbitmq',

    # Flower Option
    # broker_api = "http://guest:guest@rabbitmq:15672/api/"

else:
    log.info("Running in shell")
    # broker_url = "amqp://"

    host = socket.gethostname()
    broker_url = f"amqp://{host}:5672"

    # broker_url='pyamqp://guest:guest@localhost'
    # broker_url = 'redis://localhost:6379/0'
    result_backend = f"redis://{host}:6379/0"

    # Flower Option
    # broker_api = f"http://guest:guest@{host}:15672/api/"

    # unauthenticated_api = True
    # FLOWER_UNAUTHENTICATED_API=1 celery --config celeryconfig flower

broker_connection_retry_on_startup = True

# WORKER
imports = (("itk_demo_template.celery.tasks"),)

worker_direct = True
task_default_queue = "demi"

result_expires = 3600

# Periodic tasks setup

# beat_schedule = {
#     'bescheid-every-10-seconds': {
#         'task': 'itk_demo_template.celery.celery.health',
#         'schedule': 10.0,
#         'args': ('itk-demo-template is still alive.',),
#     },
# }

# EVENTS
worker_send_task_events = True
task_send_sent_event = True

# task_acks_late = True

# Kombu file-system broker and backend

# broker_url = 'filesystem://'
# broker_transport_options = {
#     'data_folder_in': '/app/broker/out',
#     'data_folder_out': '/app/broker/out',
#     'data_folder_processed': '/app/broker/processed',
# }
# result_persistent: False
# task_serializer: 'json'
# result_serializer: 'json'
# accept_content: ['json']
