import React, { useState, useEffect } from 'react'
import { useConfig } from '../admin-panel/config'
import { FsmBar } from '../fsmbar/fsmbar'
import {
  Page,
  PageSection,
  PageNavigation,
  Button,
  Banner
} from '@patternfly/react-core'
// import { generalRequest } from '@itk-demo-sw/utility-functions'
// Components
import {
  Navbar,
  Notifications
} from '@itk-demo-sw/components'
// Hooks
import {
  useNavbar,
  useNotifications
} from '@itk-demo-sw/hooks'
// PropTypes
import PropTypes from 'prop-types'

const TemplateDashboard = (props) => {
  const { getConfig } = useConfig()
  // const templateApiUrl = getConfig('backend')
  const templateApiUrl = process.env.REACT_APP_BASE_API_URL;

  const [
    alerts,
    onError,
    onWarn,
    onInfo,
    onSuccess,
    deleteAlert
  ] = useNotifications()

  const panels = [
    // {
    //   title: 'Dummy Panel',
    //   content: <iframe
    //     title="Swagger UI"
    //     frameBorder="0"
    //     width="100%"
    //     height="100%"
    //     src={`${props.templateUrl}/api/ui`}
    //     className="full-iframe"
    //   />
    // }
  ]

  const [activeItem, itemNames, changePanel] = useNavbar(
    panels.map((p) => p.title)
  )

  // const [fsmstate, setFsmState] = useState('')

  // const getHealth = () => {
  //   fetch(`${templateApiUrl}/health`).then(
  //     response => {
  //       // console.log(response)
  //       return response.json()
  //     }
  //   ).then(
  //     payload => {
  //       console.log(payload)
  //       setFsmState(payload.state)
  //       // return data.json()
  //     }
  //   )
  // }

  // useEffect(() => {
  //   getHealth()
  // })

  return (
    <Page>
      <PageSection padding={{ default: 'noPadding' }}>
        <PageNavigation>
          <Navbar
            activeItem={activeItem}
            changePanel={changePanel}
            itemNames={itemNames}
            onError={onError}
            onInfo={onInfo}
            onSuccess={onSuccess}
            onWarn={onWarn}
          />
        </PageNavigation>

      </PageSection>
      <Notifications alerts={alerts} deleteAlert={deleteAlert} />
      <PageSection>
        <FsmBar></FsmBar>
      </PageSection>
      <PageSection
        isFilled
        hasOverflowScroll
        padding={{ default: 'noPadding' }}
      >
        <Button onClick={() => {
          fetch(`${templateApiUrl}/run`).then(
            response => {
              // console.log(response)
              return response.json()
            }
          ).then(
            data => {
              console.log(data)
              // setFsmState(data.data)
              // return data.json()
            }
          )
        }
        }
        >Run</Button>
      </PageSection>
    </Page>
  )
}

TemplateDashboard.propTypes = {
  templateApiUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired
}

export default TemplateDashboard
