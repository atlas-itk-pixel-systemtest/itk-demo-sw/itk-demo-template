import config_checker as cc
import os

# Defines structure of config and settings files
# These classes needs to be changed depending on the fromat of the config and settings files to check

# path were config and settings files are located (configdb will.py script will cache them there as well)
FILE_DIR = os.getenv("CONFIG_DIR", "/home/itk/config")  # os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class ConfigSupport(cc.BaseConfig):
    runkey: str = "my_runkey"
    search_dict: str = '{"serial":123}'
    configdb_key: str = "demi/default/itk-demo-configdb/api/url"

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)


class Settings(cc.BaseConfig):
    config_file: str = "config.json"
    serial: int = 0

    felix_app: int = 0
    felix_card_number: int = 0
    felix_device_number: int = 0
    felix_initialize: int = 0
    dryrun: int = 0
    noflx: int = 0
    felix_data_interface: str = "lo"
    felix_toflx_ip: cc.IPType = "localhost"
    felix_tohost_ip: cc.IPType = "localhost"
    felix_tohost_dcs_pages: int = 0


class Links(cc.BaseConfig):
    polarity: int
    icec: int
    rx: list[int]
    tx: list[int]


class Config(cc.BaseConfig):
    FelixID: int
    DeviceID: int
    Links: dict[str, Links]
