#!/bin/bash

# script to be sourced to put environment variables from settings file into the current shell

old_dir=$(pwd)
cd /home/itk/api/itk_demo_template/settings/
output=$(poetry run python scripts/env.py)
vars=($output)
cd "$old_dir"

i=0
while [ $i -lt ${#vars[@]} ]
do
 export ${vars[$i]}=${vars[$i+1]}
 #echo ${vars[$i]}=${vars[$i+1]}
 ((i=i+2))
done