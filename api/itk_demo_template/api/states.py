#
# microservices template fsm states implementation
#
# in this implementation states are a hierarchy of classes
# to replicate behavior for similar states.
# state transitions are requested by sending messages to
# the input method.
# everything hardcoded, no registration of side-effects etc.
# for the time being.
#
# 2022, G. Brandt <gbrandt@cern.ch>
#

from enum import IntEnum, auto
from random import random
from time import sleep
from pprint import pprint

from connexion import request

# from flask import current_app
from rcf_response import Error

from itk_demo_template.celery import celery_app
from itk_demo_template.api.context import Context

import logging

logger = logging.getLogger("gunicorn.error")


class States(IntEnum):
    """map states to integers"""

    NONE = auto()
    INITIALIZED = auto()
    RUNNING = auto()
    DONE = auto()
    ERROR = auto()


class State:
    def __init__(self):
        self.name = ""

    """ the following two methods have no intermediate messaging system:
        interface methods are directly matched to routes.
    """

    def on_example_get(self):
        return Error(
            status=400,
            title="GET example error",
            detail="Just an example that returns an error.",
        ).to_conn_response()

    def on_example_post(self):
        payload = request.get_json()

        dic = {}
        if payload["data"] == "Hello":
            dic["data"] = "Hello back!"
        else:
            dic["data"] = "..."

        # function1(dic)

        return "OK", 200

    @staticmethod
    def input(msg):
        """handle input from intermediate messaging system"""
        return f"Not implemented: {msg}", 400


class NoneState(State):
    def __init__(self):
        self.name = "NONE"

    @staticmethod
    def input(msg):
        if msg == "request-run":
            logger.info("NONE -> INITITALIZED")
            Context.set_state(States.INITIALIZED)
            dic = "NONE -> INITITALIZED"
            return dic, 200

        return f"Not implemented: {msg}", 400


class Initialized(State):
    def __init__(self):
        self.name = "INITIALIZED"

    @staticmethod
    def input(msg):
        #        current_app.logger.info(
        #            f"{__name__} set_state {id(Context.lock)} {state.name} {Context.vcount.value}"
        #        )

        if msg == "request-run":
            async_result = celery_app.send_task(
                "function1_task",
                (1, int(100 * random())),
            )

            Context.set_state(States.RUNNING)
            dic = "INITIALIZED -> RUNNING"
            return dic, 200

        return f"Not implemented: {msg}", 400


class Running(State):
    name = "RUNNING"

    @staticmethod
    def input(msg):
        # logger.info(Running.name)
        if msg == "task-succeeded":
            #  logger.info(f"{Running.name} -> DONE")
            Context.set_state(States.DONE)
            #        while async_result.state != 'SUCCESS':
            #            print(async_result.state, async_result.info)
            #        res = async_result.get(on_message=on_raw_message, propagate=False)
            #        print(res)
            # function1()
            return "RUNNING --> DONE", 200

        if msg == "request-run":
            return "Busy", 503

        return f"Not implemented: {msg}", 400


class Done(State):
    def __init__(self):
        self.name = "DONE"

    @staticmethod
    def input(msg):
        Context.set_state(States.INITIALIZED)
        dic = {"data": "transition to Initialized"}
        return dic, 200


class ErrorState(State):
    def __init__(self):
        self.name = "ERROR"

    @staticmethod
    def input(msg):
        Context.set_state(States.INITIALIZED)
        dic = {"data": "transition to Initialized"}
        return dic, 200


Context.states_dict = {
    States.NONE: NoneState,
    States.INITIALIZED: Initialized,
    States.RUNNING: Running,
    States.DONE: Done,
    States.ERROR: ErrorState,
}

Context.set_state(States.NONE)
