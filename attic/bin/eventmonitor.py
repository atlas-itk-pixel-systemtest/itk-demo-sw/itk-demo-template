from pprint import pprint

from itk_demo_template.celery.celery import celery_app


class CeleryEventsHandler:
    def __init__(self, celery_app):
        self._app = celery_app
        self._state = celery_app.events.State()

    def _event_handler(self, event):
        self._state.event(event)
        if event["type"] != "worker-heartbeat":
            print(event["type"], event["uuid"])
        if event["type"].startswith("task-"):
            task = self._state.tasks.get(event["uuid"])

            print(task)
            # pprint(vars(task))

        # handler(self, event)

    def start_listening(self):
        with self._app.connection() as connection:
            recv = self._app.events.Receiver(
                connection, handlers={"*": self._event_handler}
            )
            recv.capture(limit=None, timeout=3600)


if __name__ == "__main__":
    events_handler = CeleryEventsHandler(celery_app)
    events_handler.start_listening()
