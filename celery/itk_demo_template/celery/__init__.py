#
# celery example module for template microservice
#
# 2022, G. Brandt <gbrandt@cern.ch>
#

from .app import celery_app

__all__ = (celery_app,)
