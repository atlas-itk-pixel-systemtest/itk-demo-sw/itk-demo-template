import createConfig from 'react-runtime-config'

export const { useConfig, useAdminConfig } = createConfig({
  namespace: 'MY_APP_CONFIG',
  schema: {
    backend: {
      type: 'string',
      title: 'Backend URL/ prefix',
      description: 'Backend URL/ prefix',
      default: '/daqapi/api'
    }
  }
})
