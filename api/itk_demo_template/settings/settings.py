from itk_demo_template.settings.settings_classes import Settings, FILE_DIR
import config_checker as cc
import os

# Allows access to settings file (read from environment or local cache) after initialy loading them
# Class is accessible from everywhere within the python project


class Felix:
    @classmethod
    def set_settings(cls):
        path = FILE_DIR + "/settings.json"
        if os.path.isfile(path):
            cls.config_sources = [cc.FileSource(file=path), cc.EnvSource(allow_all=True)]
        else:
            cls.config_sources = [cc.EnvSource(allow_all=True)]

        cls.settings = Settings(config_sources=cls.config_sources)


Felix.set_settings()
