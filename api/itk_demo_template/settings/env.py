import config_checker as cc
from itk_demo_template.settings.settings_classes import Settings, FILE_DIR

# script to load settings from file to the environment

if __name__ == "__main__":
    path = FILE_DIR + "/settings.json"

    object = Settings(config_sources=[cc.FileSource(file=path)])

    settings_dict = {
        key: value
        for key, value in object.__dict__.items()
        if not key.startswith("__") and not callable(value) and not callable(getattr(value, "__get__", None))
    }

    for key, value in settings_dict.items():
        print(key.upper())
        print(value)
