import logging
import random
import time

from itk_demo_template.celery import celery_app

from itk_demo_template.celery.backend import function1

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
logger.addHandler(ch)


@celery_app.task(name="function1_task", bind=True)
def function1_task(self, x, y):
    """Example task for asynchronous call of backend function function1"""

    # logger.info(f"{__name__} Task started")
    # Simulate a long task
    # time.sleep(random.random())
    # if not self.request.called_directly:

    for i in range(y):
        self.update_state(state="PROGRESS", meta={"step": i, "total": y})
        #        with self.app.events.default_dispatcher() as dispatcher:
        #            dispatcher.send('task-progress', field1='value1', field2=i)
        if x > 0:
            time.sleep(random.random() * x / 100.0)
            function1()

    # logger.info(f"{__name__} Task done")
    return y


@celery_app.task(name="health_task")
def health(arg):
    logger.info(f"{__name__} health {repr(arg)}")
