import React from 'react';
import ReactDOM from 'react-dom';
import '@patternfly/react-core/dist/styles/base.css';
import './style.css';
import TemplateDashboard from './screens/dashboard';
import PanelBadge from './admin-panel/panel-badge';

ReactDOM.render(
  <div className="fill-window">
    <TemplateDashboard
      key="main-content"
    //  templateApiUrl="http://localhost:5001/api"
    // templateApiUrl='http://172.25.164.195:5000/api'
    // templateApiUrl='http://template-api:5001/api'
    // templateApiUrl='http://host.docker.internal:5000/api'
    />
    {/* <PanelBadge /> */}
  </div>,
  document.getElementById('root'),
);
