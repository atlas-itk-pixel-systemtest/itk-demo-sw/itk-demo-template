import logging

from itk_demo_template.api.app import create_app
from itk_demo_template.api.events import CeleryEventsHandler

logging.basicConfig(level=logging.INFO)

gunicorn_logger = logging.getLogger("gunicorn.error")
logger = gunicorn_logger
# logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)

logger.info(f"{__name__}: Starting celery events handler")
CeleryEventsHandler().start()

logger.info(f"{__name__}: Starting asgi server")
app = create_app()

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=5001)
