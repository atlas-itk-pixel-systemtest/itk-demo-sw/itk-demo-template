#
# microservices template
# celery event handler implementation
#
# receive celery events and pass on to fsm context
#
# 2022, G. Brandt <gbrandt@cern.ch>
#

import logging
import threading

# import time
# from pprint import pprint

from itk_demo_template.celery import celery_app
from itk_demo_template.api.context import Context

# logger = logging.getLogger(__name__)
logger = logging.getLogger("gunicorn.error")


def log_task_status_change(task, event):
    logger.info(
        f"[{logger._to_datetime(task.timestamp)}] "
        f"{event['type'].upper()} {task.name} "
        f"(STATE={task.state.upper()}, UUID={task.uuid})"
    )


class CeleryEventsHandler(threading.Thread):
    """celery events handler

    runs in separate thread, needs to start in worker parent process
    """

    def __init__(self, celery_app=celery_app):
        super().__init__()
        self.daemon = True

        self._app = celery_app
        self._state = celery_app.events.State()
        self._stop_event = threading.Event()

    # def _event_handler(handler):
    #     def wrapper(self, event):
    #         self._state.event(event)
    #         task = self._state.tasks.get(event["uuid"])
    #         self._logger.log_task_status_change(task, event)
    #         if self._verbose_logging:
    #             self._logger.log_event_details(event)
    #             self._logger.log_task_details(task)
    #         handler(self, event)
    #     return wrapper

    def on_task_succeeded(self, event):
        self._state.event(event)
        # task = self._state.tasks.get(event["uuid"])
        # pprint(task)
        # log_task_status_change(task, event)

        #     fsm_state = Context.get_state_class()()
        #     # current_app.logger.info(f"{__name__}: run/ {Context.counter()} {fsm_state.name}")
        return Context.input("task-succeeded")

    def default_event_handler(self, event):
        self._state.event(event)
        # if event["type"] != "worker-heartbeat":
        #    if event["type"].startswith("task"):

        #        task = self._state.tasks.get(event["uuid"])
        #        logger.info(f"{event['type']} : {task.name}{task.args}")
        #    else:

        # logger.info(f"{event['type']}")

    def run(self):
        #        try_interval = 1
        while True:
            try:
                #                try_interval *= 2
                with self._app.connection() as connection:
                    recv = self._app.events.Receiver(
                        connection,
                        handlers={
                            "task-succeeded": self.on_task_succeeded,
                            "*": self.default_event_handler,
                        },
                    )
                    logger.info("Capturing celery events...")
                    recv.capture(limit=None, timeout=None, wakeup=True)
            except (KeyboardInterrupt, SystemExit):
                # logger.exception(e, exc_info=True)
                import _thread as thread

                thread.interrupt_main()
            except Exception as e:
                logger.error(
                    f"Failed to capture events: '{e}', "
                    #                    f"trying again in {try_interval} seconds.",
                )


#                logger.debug(e, exc_info=True)
#                time.sleep(try_interval)
