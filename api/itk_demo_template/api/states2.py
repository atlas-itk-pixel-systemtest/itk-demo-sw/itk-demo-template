from time import sleep
from random import random

from enum import IntEnum, auto
from multiprocessing import Lock, Value

from connexion import request

# from flask import current_app
from rcf_response import Error

from itk_demo_template.celery import celery_app

# SHARED STATE

current_app.logger.info(f"{__name__}: Initializing shared state")

# HOST = "127.0.0.1"
# PORT = 35791
# KEY = b"secret"
# shared_dict, shared_lock = get_shared_state(HOST, PORT, KEY)
# shared_dict["number"] = 0


class States(IntEnum):
    """map states to integers"""

    NONE = auto()
    INITIALIZING = auto()
    INITIALIZED = auto()
    RUNNING = auto()
    DONE = auto()
    ERROR = auto()


class Context:
    lock = Lock()
    states_dict = {}
    vstate = Value("i", 0)  # , lock=False)
    vcount = Value("i", 0)  # , lock=False)

    def __init__(self, state=States.NONE):
        self.set_state(state)

    @staticmethod
    def set_state(state):
        # locked = lock.acquire(block=False)
        # with Context.lock:
        with Context.vcount.get_lock():
            Context.vcount.value += 1

        with Context.vstate.get_lock():
            if Context.vstate.value != state:
                Context.vstate.value = state
                current_app.logger.info(
                    f"{__name__} set_state {id(Context.lock)} {state.name} {Context.vcount.value}"
                )

    @staticmethod
    def counter():
        return Context.vcount.value

    @staticmethod
    def get_state_name():
        return States(Context.vstate.value).name

    @staticmethod
    def get_state():
        return Context.vstate.value

    @staticmethod
    def get_state_class():
        return Context.states_dict[Context.vstate.value]


class State:
    def __init__(self):
        self.name = ""

    # no intermediate messaging system:
    # interface functions match routes

    # Info(payload=dic).to_conn_response()

    def on_example_get(self, context):
        return Error(
            status=400,
            title="GET example error",
            detail="Just an example that returns an error.",
        ).to_conn_response()

    def on_example_post(self, context):
        payload = request.get_json()

        dic = {}
        if payload["data"] == "Hello":
            dic["data"] = "Hello back!"
        else:
            dic["data"] = "..."

        # function1(dic)

        return "OK", 200

    def on_run(self, context):
        return "Accepted", 202


class NoneState(State):
    def __init__(self):
        self.name = "NONE"

    def on_run(self, context):
        current_app.logger.info(f"{__name__} on_run {self.name}")
        context.set_state(States.INITIALIZING)
        dic = {"data": "Initialization requested"}
        return dic, 200


class Initializing(State):
    def __init__(self):
        self.name = "INITIALIZING"

    def on_run(self, context):
        current_app.logger.info(f"{__name__} on_run {self.name}")

        locked = Context.lock.acquire(block=False)
        if not locked:
            current_app.logger.info(f"{__name__} Busy")
            dic = {"data": "busy initializing"}
            code = 503
        else:
            current_app.logger.info(f"{__name__} Sending task")
            for i in range(50):
                celery_app.send_task(
                    # "function1",
                    "itk_demo_template.celery.tasks.function1_task",
                    [i, 50 - i],
                )

                # context.set_state(States.INITIALIZED)
            dic = {"data": "sent tasks"}
            code = 202
        Context.lock.release()

        return dic, code


class Initialized(State):
    def __init__(self):
        self.name = "INITIALIZED"

    def on_run(self, context):
        context.set_state(States.RUNNING)
        dic = {"data": "transition to Running"}
        return dic, 200


class Running(State):
    def __init__(self):
        self.name = "RUNNING"

    def on_run(self, context):
        # sleep(random())
        context.set_state(States.DONE)
        # context.state = Done()
        # dic = {"data": "transition to Done"}
        # return dic, 200
        # function1(1,2)
        # celery_app.send_task("function1", (1, 2))
        # function1()
        return {"data": "transition to Done"}, 200


class Done(State):
    def __init__(self):
        self.name = "DONE"

    def on_run(self, context=None):
        context.set_state(States.INITIALIZED)
        dic = {"data": "transition to Initialized"}
        return dic, 200


class ErrorState(State):
    def __init__(self):
        self.name = "ERROR"

    def on_run(self, context=None):
        context.set_state(States.INITIALIZED)
        dic = {"data": "transition to Initialized"}
        return dic, 200


Context.states_dict = {
    States.NONE: NoneState,
    States.INITIALIZING: Initializing,
    States.INITIALIZED: Initialized,
    States.RUNNING: Running,
    States.DONE: Done,
    States.ERROR: ErrorState,
}
