
# FROM almalinux:9
FROM centos:7

ARG PYTHON_VERSION=3.9.6
ARG USERNAME=demi
ARG PORT=5001
ARG REPO=itk-demo-template

SHELL ["/bin/bash", "--login", "-c"]

RUN yum -y install dnf &&\
    dnf -y install epel-release &&\
    dnf -y update &&\
    dnf -y install sudo which git jq patch make gettext redis &&\
# Python build dependencies CentOS
    dnf install -y gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel gdbm-devel ncurses-devel db4-devel wget &&\
# Python build dependencies AlmaLinux 9
# RUN dnf -y install make gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel libuuid-devel gdbm-devel libnsl2-devel
    dnf -y clean all

# Add user
RUN groupadd $USERNAME &&\
    useradd -m -r -g $USERNAME $USERNAME &&\
    echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME &&\
    chmod 0440 /etc/sudoers.d/$USERNAME

USER $USERNAME
ENV HOME="/home/${USERNAME}"
WORKDIR ${HOME}

# TO DO: Install (and use) demi ?

# RUN git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi.git
# RUN echo ". $HOME/demi/setup.sh" >> $HOME/.bashrc

# Install pyenv + python + poetry
# (TO DO: use demi python init)

RUN git clone --depth=1 https://github.com/pyenv/pyenv.git .pyenv
ENV PYENV_ROOT $HOME/.pyenv
ENV PATH $PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH
ENV eval "$(pyenv init -)"

# ENV PYTHON_VERSION=3.8.10
RUN pyenv install ${PYTHON_VERSION} && pyenv local ${PYTHON_VERSION}

RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH $HOME/.local/bin:${PATH}
RUN poetry config virtualenvs.in-project true

RUN mkdir ${HOME}/itk-demo-sw
WORKDIR ${HOME}/itk-demo-sw

# Install ${REPO}
# (TO DO: use demi services)

# fake demi services setup
RUN touch ${HOME}/itk-demo-sw/.demirc

ENV dummy=1
RUN git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/${REPO}.git

WORKDIR ${HOME}/itk-demo-sw/${REPO}
RUN pyenv local ${PYTHON_VERSION}

# overwrite checked out versions by local versions for faster build refreshs
# ADD pyproject.toml .
# RUN poetry update
RUN poetry install &&\
    echo ". /home/demi/itk-demo-sw/${REPO}/.venv/bin/activate" >> $HOME/.bashrc

# COPY ./itk_demo_template itk_demo_template
# COPY ./bin bin

