#
# backend - nothing here for the time being
#
# this is where eg. pybind11 C++ bindings would go
#
# 2022 G. Brandt <gbrandt@cern.ch>
#

import logging

logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)
# ch = logging.StreamHandler()
# logger.addHandler(ch)


def function1(arg):
    logger.info(f"Called function1 with {arg}")
    return
