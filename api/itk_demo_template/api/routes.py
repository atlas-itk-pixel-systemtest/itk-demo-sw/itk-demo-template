#
# microservices template routes implementation
#
# methods are matched to endpoints via connection
# and further connect to response via different mechanisms
#
# 1. direct call
# 2. fsm with matching endpoint
# 3. fsm taking input from intermediary messaging queue
#
# 2022, G. Brandt <gbrandt@cern.ch>
#

import logging

# should not work with gunicorn
# TO DO: try NamedAtomicLock
from multiprocessing import Lock

from pprint import pprint

from itk_demo_template.celery import celery_app
from itk_demo_template.api.states import Context


logger = logging.getLogger("gunicorn.error")
logger.setLevel(logging.INFO)

# 1. Direct endpoints, not part of any FSM

health_lock = Lock()


def health():
    """health endpoint provides info on microservice status.

    read-only on context
    """

    logger.info(f"{__name__}: health/")

    dic = {}

    dic["fsm"] = {
        # FSM
        "current_state": Context.get_state_name(),
        "available_actions": ["Run", "Reset"],
        "all_states": Context.get_all_state_names(),
        "all_actions": ["Init", "Run", "Stop", "Reset"],
        "state_changes": Context.counter(),
    }

    # Querying celery is expensive so only report from one worker at a time

    locked = health_lock.acquire(False)
    if locked:
        # logger.info(f"{__name__}: health/ locked")
        try:
            cdic = {}
            logger.info(f"{__name__}: health/ inspecting Celery workers")
            ping = celery_app.control.ping(timeout=0.5)
            cdic["ping"] = ping

            i = celery_app.control.inspect()
            # cdic["registered"] = len(i.registered())
            active = i.active()
            # pprint(active)
            cdic["active"] = {k: len(v) for (k, v) in active.items()}
            cdic["reserved"] = {k: len(v) for (k, v) in i.reserved().items()}
            dic["celery"] = cdic
        except Exception as e:
            logger.error(f"{type(e).__name__} while inspecting celery app")
            dic["celery"] = "error"
        finally:
            health_lock.release()
    else:
        dic["celery"] = "busy"

    pprint(dic)
    return dic, 200


# 2. Endpoints with different implementation per FSM State


def example_get():
    return Context.get_state_class().on_example_get()


def example_post():
    return Context.get_state_class().on_example_post()


# 3. Endpoints to operate synchronous FSM by sending input to the Context


def init():
    return Context.input("request-init")


def run():
    return Context.input("request-run")


def stop():
    return Context.input("request-stop")


def reset():
    return Context.input("request-reset")
