class Input:
    """class to hold all possible inputs to the state machine"""

    def __init__(self, _input):
        self.input = _input

    def __str__(self):
        return self.input

    def __cmp__(self, other):
        return cpm(self.input, other.input)

    def __hash__(self):
        return hash(self.input)


# static input definitions

Input.on_init = Input("Initialisation requested.")
Input.on_start = Input("Start requested.")
Input.on_stop = Input("Stop requested.")
Input.on_reset = Input("Reset requested.")
