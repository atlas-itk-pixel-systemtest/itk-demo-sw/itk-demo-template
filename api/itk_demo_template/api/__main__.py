import logging
import os
import subprocess
import sys

from dotenv import load_dotenv
from rich.logging import RichHandler

from itk_demo_template.celery import celery_app

logging.basicConfig(level=logging.DEBUG, handlers=[RichHandler()])
logger = logging.getLogger(__name__)


load_dotenv()


def serve():
    API_PORT = os.environ.get("API_PORT", 5000)
    subprocess.run(
        [
            "gunicorn",
            "-k",
            "uvicorn.workers.UvicornWorker",
            f"{__package__}.asgi:app",
            "--bind",
            f"0.0.0.0:{API_PORT}",
        ]
    )


# def worker():
#     worker = celery_app.Worker(include=["itk_demo_template.celery.tasks"])
#     worker.start()

sys.exit(serve())
