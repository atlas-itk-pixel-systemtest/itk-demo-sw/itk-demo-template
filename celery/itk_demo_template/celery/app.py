#
# celery example app for template microservice
#
# 2022, G. Brandt <gbrandt@cern.ch>
#

import logging
from celery import Celery

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

celery_app = Celery("itk_demo_template")
# celery_app.config_from_object("itk_demo_template.celery.celeryconfig")

# logger.info(celery_app.conf["broker_url"])
# logger.info(celery_app.conf["broker_failover_strategy"])
# logger.info(celery_app.conf["broker_api"])
# logger.info(celery_app.conf["result_backend"])
# logger.info(celery_app.conf["result_exchange"])
